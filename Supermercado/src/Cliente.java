import java.util.Random;

public class Cliente
{
	private int numero; //numero do cliente
	private int instanteChegada;
	private int tempoAtendimento; //quantidade de tempo que resta para o cliente no caixa
	private int tempoAtendimentoS; //quantidade de tempo que resta para o cliente na sala
	private static final Random gerador = new Random();
	public static final int tempoMinAtendimento = 5;
	public static final int tempoMaxAtendimento = 10;
	public static final int tempoMinAtendimentoS = 8;
	public static final int tempoMaxAtendimentoS = 13;
	private int tipoFilme;
	
	public Cliente(int n, int c)
	{
	    numero = n;
	    instanteChegada = c;
	    tempoAtendimento = gerador.nextInt(tempoMaxAtendimento-tempoMinAtendimento+1)+tempoMinAtendimento; //gera valores entre 5 e 10
	    tempoAtendimentoS = gerador.nextInt(tempoMaxAtendimentoS-tempoMinAtendimentoS+1)+tempoMinAtendimentoS; //gera valores entre 2 e 7
	    tipoFilme = gerador.nextInt(3); //gera o valor que determina o tipo de filme escolhido pelo cliente
	    System.out.println("Cliente gerado com quantidade de compras do tipo: "+tipoFilme);
	}
	
	public int getNumero()
	{
	    return numero;
	}
	
	public int getFilme()
	{
		return tipoFilme;
	}
	
	public int getInstanteChegada()
	{
	    return instanteChegada;
	}
	
	public void decrementarTempoAtendimento()
	{
	    tempoAtendimento--;
	}
	public void decrementarTempoAtendimentoS()
	{
	    tempoAtendimentoS--;
	}
	public int getTempoAtendimento()
	{
	    return tempoAtendimento;
	}
	public int getTempoAtendimentoS()
	{
	    return tempoAtendimentoS;
	}
}
