/*
 * Classe com a logica da simulacao passo-a-passo
 */
public class Simulacao
{
	
    private static int duracao;
    private static double probabilidadeChegada;
    private QueueTAD<Cliente> fila;
    private QueueTAD<Cliente> filaComum;
    private QueueTAD<Cliente> fila3D;
    private QueueTAD<Cliente> filaIMAX;
    private Caixa caixa;
    private Salas atendenteIMAX;
    
    private Salas atendenteComum;
    
    private Salas atendente3D;
    
    private GeradorClientes geradorClientes;
    private Acumulador statTemposEsperaFila;
    private Acumulador statComprimentosFila;
    private Acumulador statTempoMedioAtendimento;
    private Acumulador statTempoMedioAtendimentoS;
    private Acumulador statTemposEsperaFilaC;
    private Acumulador statComprimentosFilaC;
    private Acumulador statTemposEsperaFila3;
    private Acumulador statComprimentosFila3;
    private Acumulador statTemposEsperaFilaI;
    private Acumulador statComprimentosFilaI;
    private boolean trace; //valor indica se a simulacao ira imprimir passo-a-passo os resultados
    
    
    private int inicioFilmes;
    private int lmtFila;
    private int limC;
    private int lim3;
    private int limI;
    private int tempoCaixaVazio=0;
    private int tempoComumVazio=0;
    private int tempo3DVazio=0;
    private int tempoIMAXVazio=0;
    private int caixaLivre=0;
    public Simulacao(boolean t,double probabilidadedeChegada,int duracao, int inicioFilmes, int lmtFila, int limC, int lim3, int limI)
    {
        fila = new QueueLinked<Cliente>();
        filaComum = new QueueLinked<Cliente>();
        fila3D = new QueueLinked<Cliente>();
        filaIMAX = new QueueLinked<Cliente>();
        caixa = new Caixa();
        atendenteComum = new Salas();
        atendente3D = new Salas();
        atendenteIMAX = new Salas();
        Simulacao.probabilidadeChegada=probabilidadedeChegada;
        geradorClientes = new GeradorClientes(probabilidadeChegada);
        statTemposEsperaFila = new Acumulador();
        statComprimentosFila = new Acumulador();
        statTemposEsperaFilaC = new Acumulador();
        statComprimentosFilaC = new Acumulador();
        statTemposEsperaFila3 = new Acumulador();
        statComprimentosFila3 = new Acumulador();
        statTemposEsperaFilaI = new Acumulador();
        statComprimentosFilaI = new Acumulador();
        statTempoMedioAtendimento=new Acumulador();
        statTempoMedioAtendimentoS=new Acumulador();
        trace = t;
        Simulacao.duracao=duracao;
        this.inicioFilmes=inicioFilmes;
        
        
        this.lmtFila= lmtFila;
        this.limC=limC;
        this.lim3=lim3;
        this.limI=limI;
        
    }
    public void sala(int tempoS,Salas atendente, QueueTAD<Cliente> filaS,int tipoS, Acumulador aTem, Acumulador aComp)
    {
    	   	if((atendente.estaVazio())&&(tempoS>=inicioFilmes))
	        {
	        	if(!filaS.isEmpty())
	        	{
	        		atendente.atenderNovoCliente(filaS.remove());
	        		aTem.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        		if(trace)
	        		{
	        			if(atendente.getClienteAtual().getFilme()==0)
	        			{
	        				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para levar compras pequenas.");
	        			}
		        		if(atendente.getClienteAtual().getFilme()==1)
		    			{
		    				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para levar compras m�dias.");
		    			}
		        		if(atendente.getClienteAtual().getFilme()==2)
		    			{
		    				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para levar compras grandes.");
		    			}
	        		}
	        	}
	        	else
	        	{
	        		switch(tipoS)
	        		{
	        		case 1:
	        			tempoComumVazio++;
	        			if(!fila3D.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(fila3D.remove());
	        				statTemposEsperaFila3.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras m�dias (atendido pelo levador pequeno).");
	        			}
	        			else if(!filaIMAX.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaIMAX.remove());
	        				statTemposEsperaFilaI.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras grandes (atendido pelo levador pequeno).");
	        			}
	        			break;
	        		case 2:
	        			tempo3DVazio++;
	        			if(!filaIMAX.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaIMAX.remove());
	        				statTemposEsperaFilaI.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras grandes (atendido pelo levador m�dio).");
	        			}
	        			else if(!filaComum.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaComum.remove());
	        				statTemposEsperaFilaC.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras pequenas (atendido pelo levador m�dio).");
	        			}
	        			break;
	        		case 3:
	        			tempoIMAXVazio++;
	        			if(!filaComum.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaComum.remove());
	        				statTemposEsperaFilaC.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras pequenas (atendido pelo levador grande).");
	        			}
	        			else if(!fila3D.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(fila3D.remove());
	        				statTemposEsperaFila3.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para levar compras m�dias (atendido pelo levador grande).");
	        			}
	        			break;
	        		}
	        	}
	        }
    	   	else if(tempoS>=inicioFilmes)
	        {
	        	if(atendente.getClienteAtual().getTempoAtendimentoS() == 0)
	        	{
	        		if(trace)
	        		{
	        			if(atendente.clienteAtual.getFilme()==0)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " termina de levar as compras pequenas.");
	        			}
	        			else if(atendente.clienteAtual.getFilme()==1)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " termina de levar as compras m�dias.");
	        			}
	        			else if(atendente.clienteAtual.getFilme()==2)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " termina de levar as compras grandes.");
	        			}
	        			
	        			
	        		}
	        		atendente.dispensarClienteAtual();
	        	}
	        	else
	            {
	                atendente.getClienteAtual().decrementarTempoAtendimentoS();
	            }
	        	aComp.adicionar(filaS.size());
	        }
    	   	else
    	   	{
    	   		aComp.adicionar(filaS.size());
    	   	}
    	
    }
    public void simular(int t)
    {
        //realizar a simulacao por um certo numero de passos de duracao
        int tempo=t;
    	if(tempo<duracao)
        {
            //verificar se um cliente chegou
            if(geradorClientes.gerar())
            {
                //se cliente chegou, criar um cliente e inserir na fila do caixa
                Cliente c = new Cliente(geradorClientes.getQuantidadeGerada(),tempo);
                if(fila.size()<=lmtFila)
                {
                	statTempoMedioAtendimento.adicionar(c.getTempoAtendimento());
	                statTempoMedioAtendimentoS.adicionar(c.getTempoAtendimentoS());
                	fila.add(c);
	                if(trace){
	                    System.out.println(tempo + ": cliente " + c.getNumero() + " ("+c.getTempoAtendimento()+" min) entra na fila - " + fila.size() + " pessoa(s)");
	                }
                }
                else
                {
                	if(trace)
                	{
                		System.out.println("Cliente gerado mas n�o pode entrar na fila. Limite de fila estourado.");
                	}
                }
            }
            //verificar se o caixa esta vazio
            if(caixa.estaVazio())
            {
                //se o caixa esta vazio, atender o primeiro cliente da fila se ele existir
                if(!fila.isEmpty())
                {
                    //tirar o cliente do inicio da fila e atender no caixa
                    caixa.atenderNovoCliente(fila.remove());
                    if(caixa.getClienteAtual().getInstanteChegada()==tempo)
                    {
                    	caixaLivre++;
                    }
                    statTemposEsperaFila.adicionar(tempo - caixa.getClienteAtual().getInstanteChegada());
                    
                    if(trace)
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " chega ao caixa.");
                }
                else
                {
                	tempoCaixaVazio++;
                }
                sala(tempo,atendenteComum,filaComum,1,statTemposEsperaFilaC,statComprimentosFilaC);
                sala(tempo,atendente3D,fila3D,2,statTemposEsperaFila3,statComprimentosFila3);
                sala(tempo,atendenteIMAX,filaIMAX,3,statTemposEsperaFilaI,statComprimentosFilaI);
                
            }
            else
            {
                //se o caixa ja esta ocupado, diminuir de um em um o tempo de atendimento ate chegar a zero
                if(caixa.getClienteAtual().getTempoAtendimento() == 0)
                {
                    if(trace)
                    {
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " deixa o caixa.");
                    }
                    if(caixa.getClienteAtual().getFilme()==0)
                    {
                    	
                    	
                    		if(filaComum.size()<=limC)
                    		{
                    		filaComum.add(caixa.getClienteAtual());
                    		
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	
                    	//caixa.checaCompra(tempo, filaComum, caixa);
                    }
                    else if(caixa.getClienteAtual().getFilme()==1)
                    {
                    	
                    		if(fila3D.size()<=lim3)
                    		{
                    			fila3D.add(caixa.getClienteAtual());
                    			
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	
                    	//caixa.checaCompra(tempo, lmt3, fila3D, caixa);
                    }
                    else if(caixa.getClienteAtual().getFilme()==2)
                    {
                    	
                    	
                    		if(filaIMAX.size()<=limI)
                    		{
                    		filaIMAX.add(caixa.getClienteAtual());
                    		
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	
                    	//caixa.checaCompra(tempo, filaIMAX, caixa);
                    }
                    
                }
                else
                {
                    caixa.getClienteAtual().decrementarTempoAtendimento();
                }
            }
            statComprimentosFila.adicionar(fila.size());
            sala(tempo,atendenteComum,filaComum,1,statTemposEsperaFilaC,statComprimentosFilaC);
            sala(tempo,atendente3D,fila3D,2,statTemposEsperaFila3,statComprimentosFila3);
            sala(tempo,atendenteIMAX,filaIMAX,3,statTemposEsperaFilaI,statComprimentosFilaI);
        }
    }
    
    public void limpar()
    {
        fila = new QueueLinked<Cliente>();
        filaComum = new QueueLinked<Cliente>();
        fila3D = new QueueLinked<Cliente>();
        filaIMAX = new QueueLinked<Cliente>();
        caixa = new Caixa();
        atendenteComum = new Salas();
        atendente3D = new Salas();
        atendenteIMAX = new Salas();
        geradorClientes = new GeradorClientes(probabilidadeChegada);
        statTemposEsperaFila = new Acumulador();
        statComprimentosFila = new Acumulador();
        statTemposEsperaFilaC = new Acumulador();
        statComprimentosFilaC = new Acumulador();
        statTemposEsperaFila3 = new Acumulador();
        statComprimentosFila3 = new Acumulador();
        statTemposEsperaFilaI = new Acumulador();
        statComprimentosFilaI = new Acumulador();
        
    }
    public int getDuracao()
    {
    	return duracao;
    }
    public int getComumSize()
    {
    	return filaComum.size();
    }
    public int get3DSize()
    {
    	return fila3D.size();
    }
    public int getIMAXSize()
    {
    	return filaIMAX.size();
    }
    public int getCaixaSize()
    {
    	return fila.size();
    }
    public int getAtendidosCaixa()
    {
    	return caixa.getNumeroAtendidos();
    }
    public int getAtendidosComum()
    {
    	return atendenteComum.getNumeroAtendidos();
    }
    public int getAtendidos3D()
    {
    	return atendente3D.getNumeroAtendidos();
    }
    public int getAtendidosIMAX()
    {
    	return atendenteIMAX.getNumeroAtendidos();
    }
    public boolean getTemgentCaixa()
    {
    	return (!caixa.estaVazio());
    }
    public boolean getTemgentComum()
    {
    	return (!atendenteComum.estaVazio());
    }
    public boolean getTemgent3D()
    {
    	return (!atendente3D.estaVazio());
    }
    public boolean getTemgentIMAX()
    {
    	return (!atendenteIMAX.estaVazio());
    }
    public void imprimirResultados()
    {
        System.out.println();
        System.out.println("Resultados da Simulacao\n");
        System.out.println("Duracao:" + duracao);
        System.out.println("Momento do in�cio dos levadores de compras:"+inicioFilmes);
        System.out.println("Probabilidade de chegada de clientes:" + probabilidadeChegada);
        System.out.println("Tamanho m�ximo para a fila do caixa:"+lmtFila);
        System.out.println("Tamanho m�ximo para a fila do levador pequeno:"+limC);
        System.out.println("Tamanho m�ximo para a fila do levador m�dio:"+lim3);
        System.out.println("Tamanho m�ximo para a fila do levador grande:"+limI);
        System.out.println("Tempo em que a fila do caixa esteve vazia:"+tempoCaixaVazio);
        System.out.println("Tempo em que a fila do levador pequeno esteve vazia:"+tempoComumVazio);
        System.out.println("Tempo em que a fila do levador m�dio esteve vazia:"+tempo3DVazio);
        System.out.println("Tempo em que a fila do levador grande esteve vazia:"+tempoIMAXVazio);
        System.out.println("Quantidade de clientes que foram para o caixa sem esperar em fila:"+caixaLivre);
        System.out.println("Tempo de atendimento minimo nos caixas:" + Cliente.tempoMinAtendimento);
        System.out.println("Tempo de atendimento maximo nos caixas:" + Cliente.tempoMaxAtendimento);
        System.out.println("Tempo de atendimento minimo nos levadores:" + Cliente.tempoMinAtendimentoS);
        System.out.println("Tempo de atendimento maximo nos levadores:" + Cliente.tempoMaxAtendimentoS);
        System.out.println("Tempo m�dio de atendimento no caixa: "+statTempoMedioAtendimento.getMedia());
        System.out.println("Tempo m�dio de atendimento nos levadores: "+statTempoMedioAtendimentoS.getMedia());
        System.out.println("Clientes atendidos pelo caixa:" + caixa.getNumeroAtendidos());
        System.out.println("Clientes atendidos pelo levador pequeno:" + atendenteComum.getNumeroAtendidos());
        System.out.println("Clientes atendidos pelo levador m�dio:" + atendente3D.getNumeroAtendidos());
        System.out.println("Clientes atendidos pelo levador grande:" + atendenteIMAX.getNumeroAtendidos());
        System.out.println("Clientes ainda na fila do caixa:" + fila.size());
        System.out.println("Clientes ainda na fila do levador pequeno:" + filaComum.size());
        System.out.println("Clientes ainda na fila do levador m�dio:" + fila3D.size());
        System.out.println("Clientes ainda na fila do levador grande:" + filaIMAX.size());
        System.out.println("Cliente ainda no caixa:" + (caixa.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila do levador pequeno:" + (atendenteComum.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila do levador m�dio:" + (atendente3D.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila do levador grande:" + (atendenteIMAX.getClienteAtual() != null));
        System.out.println("Total de clientes gerados:" + geradorClientes.getQuantidadeGerada());
        System.out.println("Tempo medio de espera no caixa:" + statTemposEsperaFila.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para o levador pequeno:" + statTemposEsperaFilaC.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para o levador m�dio:" + statTemposEsperaFila3.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para o levador grande:" + statTemposEsperaFilaI.getMedia());
        System.out.println("Comprimento medio da fila do caixa:" + statComprimentosFila.getMedia());
        System.out.println("Comprimento medio da fila do levador pequeno:" + statComprimentosFilaC.getMedia());
        System.out.println("Comprimento medio da fila do levador m�dio:" + statComprimentosFila3.getMedia());
        System.out.println("Comprimento medio da fila do levador grande:" + statComprimentosFilaI.getMedia());
        
    }
}
