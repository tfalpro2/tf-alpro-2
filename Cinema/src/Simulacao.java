/*
 * Classe com a logica da simulacao passo-a-passo
 */
public class Simulacao
{
	
    private static int duracao;
    private static double probabilidadeChegada;
    private QueueTAD<Cliente> fila;
    private QueueTAD<Cliente> filaComum;
    private QueueTAD<Cliente> fila3D;
    private QueueTAD<Cliente> filaIMAX;
    private Caixa caixa;
    private Salas atendenteIMAX;
    private int lmtIMAX;
    private Salas atendenteComum;
    private int lmtComum;
    private Salas atendente3D;
    private int lmt3D;
    private GeradorClientes geradorClientes;
    private Acumulador statTemposEsperaFila;
    private Acumulador statComprimentosFila;
    private Acumulador statTempoMedioAtendimento;
    private Acumulador statTempoMedioAtendimentoS;
    private Acumulador statTemposEsperaFilaC;
    private Acumulador statComprimentosFilaC;
    private Acumulador statTemposEsperaFila3;
    private Acumulador statComprimentosFila3;
    private Acumulador statTemposEsperaFilaI;
    private Acumulador statComprimentosFilaI;
    private boolean trace; //valor indica se a simulacao ira imprimir passo-a-passo os resultados
    private int lmtC;
    private int lmt3;
    private int lmtI;
    private int lmtEst=0;
    private int inicioFilmes;
    private int lmtFila;
    private int limC;
    private int lim3;
    private int limI;
    private int tempoCaixaVazio=0;
    private int tempoComumVazio=0;
    private int tempo3DVazio=0;
    private int tempoIMAXVazio=0;
    private int caixaLivre=0;
    public Simulacao(boolean t,int limiteImax,int limite3D,int limiteComum,double probabilidadedeChegada,int duracao, int inicioFilmes, int lmtFila, int limC, int lim3, int limI)
    {
        fila = new QueueLinked<Cliente>();
        filaComum = new QueueLinked<Cliente>();
        fila3D = new QueueLinked<Cliente>();
        filaIMAX = new QueueLinked<Cliente>();
        caixa = new Caixa();
        atendenteComum = new Salas();
        atendente3D = new Salas();
        atendenteIMAX = new Salas();
        Simulacao.probabilidadeChegada=probabilidadedeChegada;
        geradorClientes = new GeradorClientes(probabilidadeChegada);
        statTemposEsperaFila = new Acumulador();
        statComprimentosFila = new Acumulador();
        statTemposEsperaFilaC = new Acumulador();
        statComprimentosFilaC = new Acumulador();
        statTemposEsperaFila3 = new Acumulador();
        statComprimentosFila3 = new Acumulador();
        statTemposEsperaFilaI = new Acumulador();
        statComprimentosFilaI = new Acumulador();
        statTempoMedioAtendimento=new Acumulador();
        statTempoMedioAtendimentoS=new Acumulador();
        trace = t;
        Simulacao.duracao=duracao;
        this.inicioFilmes=inicioFilmes;
        this.lmtIMAX=limiteImax;
        this.lmt3D=limite3D;
        this.lmtComum=limiteComum;
        this.lmtC = lmtComum;
        this.lmt3 = lmt3D;
        this.lmtI = lmtIMAX;
        this.lmtFila= lmtFila;
        this.limC=limC;
        this.lim3=lim3;
        this.limI=limI;
        
    }
    public void sala(int tempoS,Salas atendente, QueueTAD<Cliente> filaS,int tipoS, Acumulador aTem, Acumulador aComp)
    {
    	   	if((atendente.estaVazio())&&(tempoS>=inicioFilmes))
	        {
	        	if(!filaS.isEmpty())
	        	{
	        		atendente.atenderNovoCliente(filaS.remove());
	        		aTem.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        		if(trace)
	        		{
	        			if(atendente.getClienteAtual().getFilme()==0)
	        			{
	        				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para a sala comum.");
	        			}
		        		if(atendente.getClienteAtual().getFilme()==1)
		    			{
		    				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para a sala 3D.");
		    			}
		        		if(atendente.getClienteAtual().getFilme()==2)
		    			{
		    				System.out.println(tempoS + ": cliente "+atendente.getClienteAtual().getNumero() + " chega ao atendente para a sala IMAX.");
		    			}
	        		}
	        	}
	        	else
	        	{
	        		switch(tipoS)
	        		{
	        		case 1:
	        			tempoComumVazio++;
	        			if(!fila3D.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(fila3D.remove());
	        				statTemposEsperaFila3.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala 3D (atendido pelo atendente da sala comum).");
	        			}
	        			else if(!filaIMAX.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaIMAX.remove());
	        				statTemposEsperaFilaI.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala IMAX (atendido pelo atendente da sala comum).");
	        			}
	        			break;
	        		case 2:
	        			tempo3DVazio++;
	        			if(!filaIMAX.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaIMAX.remove());
	        				statTemposEsperaFilaI.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala IMAX (atendido pelo atendente da sala 3D).");
	        			}
	        			else if(!filaComum.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaComum.remove());
	        				statTemposEsperaFilaC.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala comum (atendido pelo atendente da sala 3D).");
	        			}
	        			break;
	        		case 3:
	        			tempoIMAXVazio++;
	        			if(!filaComum.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(filaComum.remove());
	        				statTemposEsperaFilaC.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala comum (atendido pelo atendente da sala IMAX).");
	        			}
	        			else if(!fila3D.isEmpty())
	        			{
	        				atendente.atenderNovoCliente(fila3D.remove());
	        				statTemposEsperaFila3.adicionar(tempoS - atendente.getClienteAtual().getInstanteChegada());
	        				System.out.println(tempoS+": cliente "+atendente.getClienteAtual().getNumero()+" chega ao atendente para a sala 3D (atendido pelo atendente da sala IMAX).");
	        			}
	        			break;
	        		}
	        	}
	        }
    	   	else if(tempoS>=inicioFilmes)
	        {
	        	if(atendente.getClienteAtual().getTempoAtendimentoS() == 0)
	        	{
	        		if(trace)
	        		{
	        			if(atendente.clienteAtual.getFilme()==0)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " entra na sala comum.");
	        			}
	        			else if(atendente.clienteAtual.getFilme()==1)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " entra na sala 3D.");
	        			}
	        			else if(atendente.clienteAtual.getFilme()==2)
	        			{
	        				System.out.println(tempoS+ ": cliente " + atendente.getClienteAtual().getNumero() + " entra na sala IMAX.");
	        			}
	        			
	        			
	        		}
	        		atendente.dispensarClienteAtual();
	        	}
	        	else
	            {
	                atendente.getClienteAtual().decrementarTempoAtendimentoS();
	            }
	        	aComp.adicionar(filaS.size());
	        }
    	   	else
    	   	{
    	   		aComp.adicionar(filaS.size());
    	   	}
    	
    }
    public void simular(int t)
    {
        //realizar a simulacao por um certo numero de passos de duracao
        int tempo=t;
    	if(tempo<duracao)
        {
            //verificar se um cliente chegou
            if(geradorClientes.gerar())
            {
                //se cliente chegou, criar um cliente e inserir na fila do caixa
                Cliente c = new Cliente(geradorClientes.getQuantidadeGerada(),tempo);
                if(fila.size()<=lmtFila)
                {
                	statTempoMedioAtendimento.adicionar(c.getTempoAtendimento());
	                statTempoMedioAtendimentoS.adicionar(c.getTempoAtendimentoS());
                	fila.add(c);
	                if(trace){
	                    System.out.println(tempo + ": cliente " + c.getNumero() + " ("+c.getTempoAtendimento()+" min) entra na fila - " + fila.size() + " pessoa(s)");
	                }
                }
                else
                {
                	if(trace)
                	{
                		System.out.println("Cliente gerado mas n�o pode entrar na fila. Limite de fila estourado.");
                	}
                }
            }
            //verificar se o caixa esta vazio
            if(caixa.estaVazio())
            {
                //se o caixa esta vazio, atender o primeiro cliente da fila se ele existir
                if(!fila.isEmpty())
                {
                    //tirar o cliente do inicio da fila e atender no caixa
                    caixa.atenderNovoCliente(fila.remove());
                    if(caixa.getClienteAtual().getInstanteChegada()==tempo)
                    {
                    	caixaLivre++;
                    }
                    statTemposEsperaFila.adicionar(tempo - caixa.getClienteAtual().getInstanteChegada());
                    
                    if(trace)
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " chega ao caixa.");
                }
                else
                {
                	tempoCaixaVazio++;
                }
                sala(tempo,atendenteComum,filaComum,1,statTemposEsperaFilaC,statComprimentosFilaC);
                sala(tempo,atendente3D,fila3D,2,statTemposEsperaFila3,statComprimentosFila3);
                sala(tempo,atendenteIMAX,filaIMAX,3,statTemposEsperaFilaI,statComprimentosFilaI);
                
            }
            else
            {
                //se o caixa ja esta ocupado, diminuir de um em um o tempo de atendimento ate chegar a zero
                if(caixa.getClienteAtual().getTempoAtendimento() == 0)
                {
                    if(trace)
                    {
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " deixa o caixa.");
                    }
                    if(caixa.getClienteAtual().getFilme()==0)
                    {
                    	if(lmtC==0)
                    	{
                    		caixa.dispensarClienteAtual();
                    		System.out.println(tempo +": cliente "+caixa.getClienteAtual().getNumero() + " n�o comprou ingressos por falta de lugares na sala.");
                    		lmtEst++;
                    	}
                    	else
                    	{
                    		if(filaComum.size()<=limC)
                    		{
                    		filaComum.add(caixa.getClienteAtual());
                    		lmtC--;
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	}
                    	//caixa.checaCompra(tempo, lmtC, lmtEst, filaComum, caixa);
                    }
                    else if(caixa.getClienteAtual().getFilme()==1)
                    {
                    	if(lmt3==0)
                    	{
                    		caixa.dispensarClienteAtual();
                    		System.out.println(tempo +": cliente "+caixa.getClienteAtual().getNumero() + " n�o comprou ingressos por falta de lugares na sala.");
                    		lmtEst++;
                    	}
                    	else
                    	{
                    		if(fila3D.size()<=lim3)
                    		{
                    			fila3D.add(caixa.getClienteAtual());
                    			lmt3--;
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	}
                    	//caixa.checaCompra(tempo, lmt3, lmtEst, fila3D, caixa);
                    }
                    else if(caixa.getClienteAtual().getFilme()==2)
                    {
                    	if(lmtI==0)
                    	{
                    		caixa.dispensarClienteAtual();
                    		System.out.println(tempo +": cliente "+caixa.getClienteAtual().getNumero() + " n�o comprou ingressos por falta de lugares na sala.");
                    		lmtEst++;
                    	}
                    	else
                    	{
                    		if(filaIMAX.size()<=limI)
                    		{
                    		filaIMAX.add(caixa.getClienteAtual());
                    		lmtI--;
                    		}
                    		caixa.dispensarClienteAtual();
                            
                    	}
                    	//caixa.checaCompra(tempo, lmtI, lmtEst, filaIMAX, caixa);
                    }
                    
                }
                else
                {
                    caixa.getClienteAtual().decrementarTempoAtendimento();
                }
            }
            statComprimentosFila.adicionar(fila.size());
            sala(tempo,atendenteComum,filaComum,1,statTemposEsperaFilaC,statComprimentosFilaC);
            sala(tempo,atendente3D,fila3D,2,statTemposEsperaFila3,statComprimentosFila3);
            sala(tempo,atendenteIMAX,filaIMAX,3,statTemposEsperaFilaI,statComprimentosFilaI);
        }
    }
    
    public void limpar()
    {
        fila = new QueueLinked<Cliente>();
        filaComum = new QueueLinked<Cliente>();
        fila3D = new QueueLinked<Cliente>();
        filaIMAX = new QueueLinked<Cliente>();
        caixa = new Caixa();
        atendenteComum = new Salas();
        atendente3D = new Salas();
        atendenteIMAX = new Salas();
        geradorClientes = new GeradorClientes(probabilidadeChegada);
        statTemposEsperaFila = new Acumulador();
        statComprimentosFila = new Acumulador();
        statTemposEsperaFilaC = new Acumulador();
        statComprimentosFilaC = new Acumulador();
        statTemposEsperaFila3 = new Acumulador();
        statComprimentosFila3 = new Acumulador();
        statTemposEsperaFilaI = new Acumulador();
        statComprimentosFilaI = new Acumulador();
        lmtC=lmtComum;
        lmt3=lmt3D;
        lmtI=lmtIMAX;
        lmtEst=0;
    }
    public int getDuracao()
    {
    	return duracao;
    }
    public int getComumSize()
    {
    	return filaComum.size();
    }
    public int get3DSize()
    {
    	return fila3D.size();
    }
    public int getIMAXSize()
    {
    	return filaIMAX.size();
    }
    public int getCaixaSize()
    {
    	return fila.size();
    }
    public int getAtendidosCaixa()
    {
    	return caixa.getNumeroAtendidos();
    }
    public int getAtendidosComum()
    {
    	return atendenteComum.getNumeroAtendidos();
    }
    public int getAtendidos3D()
    {
    	return atendente3D.getNumeroAtendidos();
    }
    public int getAtendidosIMAX()
    {
    	return atendenteIMAX.getNumeroAtendidos();
    }
    public boolean getTemgentCaixa()
    {
    	return (!caixa.estaVazio());
    }
    public boolean getTemgentComum()
    {
    	return (!atendenteComum.estaVazio());
    }
    public boolean getTemgent3D()
    {
    	return (!atendente3D.estaVazio());
    }
    public boolean getTemgentIMAX()
    {
    	return (!atendenteIMAX.estaVazio());
    }
    public void imprimirResultados()
    {
        System.out.println();
        System.out.println("Resultados da Simulacao\n");
        System.out.println("Duracao:" + duracao);
        System.out.println("Momento da abertura das salas:"+inicioFilmes);
        System.out.println("Probabilidade de chegada de clientes:" + probabilidadeChegada);
        System.out.println("Tamanho m�ximo para a fila do caixa:"+lmtFila);
        System.out.println("Tamanho m�ximo para a fila da sala comum:"+limC);
        System.out.println("Tamanho m�ximo para a fila da sala 3D:"+lim3);
        System.out.println("Tamanho m�ximo para a fila da sala IMAX:"+limI);
        System.out.println("Tempo em que a fila do caixa esteve vazia:"+tempoCaixaVazio);
        System.out.println("Tempo em que a fila da sala comum esteve vazia:"+tempoComumVazio);
        System.out.println("Tempo em que a fila da sala 3D esteve vazia:"+tempo3DVazio);
        System.out.println("Tempo em que a fila da sala IMAX esteve vazia:"+tempoIMAXVazio);
        System.out.println("Quantidade de clientes que foram para o caixa sem esperar em fila:"+caixaLivre);
        System.out.println("Tempo de atendimento minimo nos caixas:" + Cliente.tempoMinAtendimento);
        System.out.println("Tempo de atendimento maximo nos caixas:" + Cliente.tempoMaxAtendimento);
        System.out.println("Tempo de atendimento minimo nas salas:" + Cliente.tempoMinAtendimentoS);
        System.out.println("Tempo de atendimento maximo nas salas:" + Cliente.tempoMaxAtendimentoS);
        System.out.println("Tempo m�dio de atendimento no caixa: "+statTempoMedioAtendimento.getMedia());
        System.out.println("Tempo m�dio de atendimento nas salas: "+statTempoMedioAtendimentoS.getMedia());
        System.out.println("Clientes atendidos pelo caixa:" + caixa.getNumeroAtendidos());
        System.out.println("Clientes atendidos pela sala comum:" + atendenteComum.getNumeroAtendidos());
        System.out.println("Clientes atendidos pela sala 3D:" + atendente3D.getNumeroAtendidos());
        System.out.println("Clientes atendidos pela sala IMAX:" + atendenteIMAX.getNumeroAtendidos());
        System.out.println("Clientes ainda na fila do caixa:" + fila.size());
        System.out.println("Clientes ainda na fila da sala comum:" + filaComum.size());
        System.out.println("Clientes ainda na fila da sala 3D:" + fila3D.size());
        System.out.println("Clientes ainda na fila da sala IMAX:" + filaIMAX.size());
        System.out.println("Cliente ainda no caixa:" + (caixa.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila da sala comum:" + (atendenteComum.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila da sala 3D:" + (atendente3D.getClienteAtual() != null));
        System.out.println("Cliente ainda na fila da sala IMAX:" + (atendenteIMAX.getClienteAtual() != null));
        System.out.println("Total de clientes gerados:" + geradorClientes.getQuantidadeGerada());
        System.out.println("Tempo medio de espera no caixa:" + statTemposEsperaFila.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para a sala comum:" + statTemposEsperaFilaC.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para a sala 3D:" + statTemposEsperaFila3.getMedia());
        System.out.println("Tempo medio de espera para os clientes que foram para a sala IMAX:" + statTemposEsperaFilaI.getMedia());
        System.out.println("Comprimento medio da fila do caixa:" + statComprimentosFila.getMedia());
        System.out.println("Comprimento medio da fila da sala comum:" + statComprimentosFilaC.getMedia());
        System.out.println("Comprimento medio da fila da sala 3D:" + statComprimentosFila3.getMedia());
        System.out.println("Comprimento medio da fila da sala IMAX:" + statComprimentosFilaI.getMedia());
        System.out.println("Limite de pessoas para a sala comum:"+lmtComum);
        System.out.println("Limite de pessoas para a sala 3D:"+lmt3D);
        System.out.println("Limite de pessoas para a sala IMAX:"+lmtIMAX);
        System.out.println("Quantidade de clientes que foram dispensados por limite de sala estourado:"+lmtEst);
    }
}
