public class Caixa extends Salas
{

	public Caixa()
	{
	    clienteAtual = null;
	    numeroAtendidos = 0;
	}

	public void checaCompra(int tempo,int lmt,int Est,QueueTAD<Cliente> fila, Caixa caixa)
	{
		if(lmt==0)
    	{
    		caixa.dispensarClienteAtual();
    		System.out.println(tempo +": cliente "+caixa.getClienteAtual().getNumero() + " n�o comprou ingressos por falta de lugares na sala.");
    		Est++;
    	}
    	else
    	{
    		fila.add(caixa.getClienteAtual());
            caixa.dispensarClienteAtual();
            lmt--;
    	}
	}

}
