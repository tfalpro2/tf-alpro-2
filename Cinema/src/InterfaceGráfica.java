import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;

import java.awt.CardLayout;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JLabel;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTable;
import java.awt.Font;
import java.awt.GridLayout;


public class InterfaceGráfica extends JFrame {

	private JPanel contentPane;
	int x =0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		JSONObject arq;
    	JSONParser parser = new JSONParser();
    	
    	Long limiteImax1 = null;
    	Long limite3D1 = null; 
    	Long limiteComum1 = null;
    	Long duracao1 = null;
    	Long iniFilmes1 = null;
    	Long limFilas1 = null;
    	Long limC1 = null;
    	Long lim31 = null;
    	Long limI1 = null;
    	double probabilidadedeChegada=0;
    	try{
    		arq = (JSONObject) parser.parse(new FileReader ("arquivo.json"));
    		limiteImax1 =  (Long) arq.get("limiteImax");
    		limite3D1 = (Long) arq.get("limite3D");
    		limiteComum1 = (Long) arq.get("limiteComum");
    		probabilidadedeChegada = (Double) arq.get("probabilidadeDeChegada");
    		duracao1 = (Long) arq.get("duracao");
    		iniFilmes1 = (Long) arq.get("momentoAberturaSalas");
    		limFilas1 = (Long) arq.get("limiteMaxFilas");
    		limC1 = (Long) arq.get("limiteMaxComum");
    		lim31 = (Long) arq.get("limiteMax3D");
    		limI1 = (Long) arq.get("limiteMaxIMAX");
    		
    	}
    	catch(FileNotFoundException e1){
    		System.out.println("Erro de E/S"); 
    	}
    	catch(IOException e){
    		System.out.println("Erro de E/S"); 
    	}
    	
    	catch(ParseException e){
    		System.out.println("Erro de E/S"); 
    	}
    	Integer limiteImax= new Integer(limiteImax1.intValue());
    	Integer limite3D= new Integer(limite3D1.intValue());
    	Integer limiteComum= new Integer(limiteComum1.intValue());
    	Integer duracao= new Integer(duracao1.intValue());
    	Integer iniFilmes= new Integer(iniFilmes1.intValue());
    	Integer lmtFilas = new Integer(limFilas1.intValue());
    	Integer limC = new Integer(limC1.intValue());
    	Integer lim3 = new Integer(lim31.intValue());
    	Integer limI = new Integer(limI1.intValue());
    	
        final Simulacao sim = new Simulacao(true,limiteImax,limite3D,limiteComum,probabilidadedeChegada,duracao,iniFilmes,lmtFilas,limC,lim3,limI);
        
        InterfaceGráfica ig = new InterfaceGráfica(sim);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceGráfica frame = new InterfaceGráfica(sim);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfaceGráfica(final Simulacao sim) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 553, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		
		final JLabel aindac = new JLabel("");
		aindac.setBounds(123, 141, 99, 44);
		panel.add(aindac);
		aindac.setForeground(Color.BLACK);
		aindac.setBackground(Color.WHITE);
		
		final JLabel ainda3 = new JLabel("");
		ainda3.setBounds(123, 169, 101, 44);
		panel.add(ainda3);
		ainda3.setForeground(Color.BLACK);
		ainda3.setBackground(Color.WHITE);
		
		final JLabel aindai = new JLabel("");
		aindai.setBounds(123, 198, 99, 44);
		panel.add(aindai);
		aindai.setForeground(Color.BLACK);
		aindai.setBackground(Color.WHITE);
		
		final JLabel aindax = new JLabel("");
		aindax.setBounds(123, 106, 34, 50);
		panel.add(aindax);
		
		JButton btnNewButton = new JButton("Etapa");
		btnNewButton.setBounds(10, 22, 89, 23);
		panel.add(btnNewButton);
		
		JLabel lblDurao = new JLabel("Dura\u00E7\u00E3o:");
		lblDurao.setBounds(123, 8, 56, 50);
		panel.add(lblDurao);
		
		final JLabel dur = new JLabel(""+sim.getDuracao());
		dur.setBounds(189, 11, 110, 44);
		panel.add(dur);
		dur.setBackground(Color.WHITE);
		dur.setForeground(Color.BLACK);
		
		JLabel lblClientesAtendidos = new JLabel("Clientes atendidos:");
		lblClientesAtendidos.setBounds(246, 59, 121, 50);
		panel.add(lblClientesAtendidos);
		
		JLabel lblPessoasAindaNa = new JLabel("Pessoas na fila:");
		lblPessoasAindaNa.setBounds(123, 59, 144, 50);
		panel.add(lblPessoasAindaNa);
		
		JLabel lblLikl = new JLabel("Caixa de compras");
		lblLikl.setBounds(10, 106, 113, 50);
		panel.add(lblLikl);
		
		JLabel lblComum = new JLabel("Comum");
		lblComum.setBounds(10, 135, 44, 50);
		panel.add(lblComum);
		
		JLabel lbld = new JLabel("3D");
		lbld.setBounds(10, 163, 44, 50);
		panel.add(lbld);
		
		JLabel lblImax = new JLabel("IMAX");
		lblImax.setBounds(10, 192, 44, 50);
		panel.add(lblImax);
		
		final JLabel atendidox = new JLabel("");
		atendidox.setBounds(256, 106, 200, 50);
		panel.add(atendidox);
		
		final JLabel atendidoc = new JLabel("");
		atendidoc.setBounds(256, 135, 200, 50);
		panel.add(atendidoc);
		
		final JLabel atendido3 = new JLabel("");
		atendido3.setBounds(256, 169, 200, 50);
		panel.add(atendido3);
		
		final JLabel atendidoi = new JLabel("");
		atendidoi.setBounds(256, 192, 200, 50);
		panel.add(atendidoi);
		
		JLabel lblTempoAtual = new JLabel("Tempo atual:");
		lblTempoAtual.setBounds(246, 8, 200, 50);
		panel.add(lblTempoAtual);
		
		final JLabel temp = new JLabel("");
		temp.setBounds(331, 8, 200, 50);
		panel.add(temp);
		
		JLabel lblClienteSendoAtendido = new JLabel("Cliente sendo atendido:");
		lblClienteSendoAtendido.setBounds(377, 59, 200, 50);
		panel.add(lblClienteSendoAtendido);
		
		final JLabel sendox = new JLabel("");
		sendox.setBounds(387, 106, 200, 50);
		panel.add(sendox);
		
		final JLabel sendoc = new JLabel("");
		sendoc.setBounds(386, 133, 200, 50);
		panel.add(sendoc);
		
		final JLabel sendo3 = new JLabel("");
		sendo3.setBounds(385, 162, 200, 50);
		panel.add(sendo3);
		
		final JLabel sendoi = new JLabel("");
		sendoi.setBounds(387, 192, 200, 50);
		panel.add(sendoi);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				sim.simular(x);
				x++;
				aindac.setText(""+sim.getComumSize());
				aindax.setText(""+sim.getCaixaSize());
				aindai.setText(""+sim.getIMAXSize());
				ainda3.setText(""+sim.get3DSize());
				atendidox.setText(""+sim.getAtendidosCaixa());
				atendidoc.setText(""+sim.getAtendidosComum());
				atendido3.setText(""+sim.getAtendidos3D());
				atendidoi.setText(""+sim.getAtendidosIMAX());
				if(x<=sim.getDuracao()){temp.setText(""+x);}
				sendox.setText(""+sim.getTemgentCaixa());
				sendoc.setText(""+sim.getTemgentComum());
				sendo3.setText(""+sim.getTemgent3D());
				sendoi.setText(""+sim.getTemgentIMAX());
				if(x==sim.getDuracao())
				{
					sim.imprimirResultados();
				}
				
			}
		});

		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sim.limpar();
				x=0;
				aindac.setText(""+sim.getComumSize());
				aindax.setText(""+sim.getCaixaSize());
				aindai.setText(""+sim.getIMAXSize());
				ainda3.setText(""+sim.get3DSize());
				atendidox.setText(""+sim.getAtendidosCaixa());
				atendidoc.setText(""+sim.getAtendidosComum());
				atendido3.setText(""+sim.getAtendidos3D());
				atendidoi.setText(""+sim.getAtendidosIMAX());
				if(x<=sim.getDuracao()){temp.setText(""+x);}
				sendox.setText(""+sim.getTemgentCaixa());
				sendoc.setText(""+sim.getTemgentComum());
				sendo3.setText(""+sim.getTemgent3D());
				sendoi.setText(""+sim.getTemgentIMAX());
			}
		});
		btnLimpar.setBounds(10, 73, 89, 23);
		panel.add(btnLimpar);
		
	}
}
